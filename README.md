# README #

# Необходимо установить!
* Python 2.7* https://www.python.org/

```bash
# Устанавливаем утилиту virtualenv для создания "песочницы" нашего проеткта
pip install virtualenv

# В папке с проектом
virtualenv venv

# Устанавливаем библиотеки
venv\Scripts\pip install -r server/requirements.txt
```

* PostgreSQL https://www.postgresql.org/download/ и выполнить SQL запрос из под пользователя postgres

```bash

CREATE ROLE remote LOGIN
  ENCRYPTED PASSWORD 'md54f681a45d4bf09c36a81f067963e640b'
  NOSUPERUSER INHERIT CREATEDB NOCREATEROLE NOREPLICATION;

CREATE DATABASE "web-template"
  WITH OWNER = remote
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Russian_Russia.1251'
       LC_CTYPE = 'Russian_Russia.1251'
       CONNECTION LIMIT = -1;

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public."user"
(
  id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
  active boolean,
  email character varying(255),
  password character varying(255),
  CONSTRAINT user_pkey PRIMARY KEY (id),
  CONSTRAINT user_email_key UNIQUE (email)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."user"
  OWNER TO remote;

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.role
(
  id integer NOT NULL DEFAULT nextval('role_id_seq'::regclass),
  name character varying(80),
  description character varying(255),
  CONSTRAINT role_pkey PRIMARY KEY (id),
  CONSTRAINT role_name_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.role
  OWNER TO remote;

CREATE TABLE public.roles_users
(
  user_id integer,
  role_id integer,
  CONSTRAINT roles_users_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES public.role (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT roles_users_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES public."user" (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.roles_users
  OWNER TO remote;

INSERT INTO public.role(
            id, name, description)
    VALUES (1, 'admin', 'Администратор');
INSERT INTO public."user"(
            id, active, email, password)
    VALUES (1, True, 'admin', '$2a$12$btpBJsDA0Coy1kVj3O9tKOuMvOigFFXVHR4O3.bpMPuZr5LE.BRDu');

INSERT INTO public.roles_users(
            user_id, role_id)
    VALUES (1, 1);


```

* NodeJS https://nodejs.org/en/ и выполнить команды

```bash
npm install -g gulp
npm install
```

# Запуск
```bash
# запуск gulp и python
npm start # через hamachi
npm run start-local # в локальной сети
```


# Про фронтенд
Собирается все gulp'ом в папку `dist/`. Флаг `--production` собирает в `dist-production`.

## Таски:
* `gulp vendor` - собирает файлы библиотек из `src/vendor`
* `gulp css` - Собирает Stylus из папки `src/css/*.styl`.
* `gulp css --production` - то же самое + минификация
* `gulp copy-glyphicons` - шрифт для бутстрапа из npm
* `gulp img` - сжимает картинки из `src/img`, 10 шакалов из 10
* `gulp js` - собирает js из `src/js/*.js` c помощью `rollup`.
* `gulp js --production` - то же самое + минификация
* `gulp fonts` - собирает шрифты из Google Fonts

## Опции сервера
```shell
python server/server.py --help                                                                                                                                                                 0 ms  master 
usage: server.py [-h] [--db_host DB_HOST] [--db_user DB_USER]
                 [--db_pass DB_PASS] [--db_name DB_NAME]
                 [--postgres_driver POSTGRES_DRIVER] [--debug DEBUG]

optional arguments:
  -h, --help            show this help message and exit
  --db_host DB_HOST     PostgreSQL address, <ip>:<port>
  --db_user DB_USER     PostgreSQL username
  --db_pass DB_PASS     PostgreSQL password
  --db_name DB_NAME     PostgreSQL db name
  --postgres_driver POSTGRES_DRIVER
                        PostgreSQL python driver, psycopg2 (fast, c++), pg8000
                        (slower, python)
  --debug DEBUG         toggle debug mode
```