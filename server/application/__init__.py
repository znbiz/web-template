# -*- coding: utf-8 -*-
from flask import Flask
from argparse import ArgumentParser
from extensions import db
from models.security import User, Role
from flask_security import Security, SQLAlchemyUserDatastore
import os
import sys

#########################################################################

cmdline_args_parser = ArgumentParser()

# Конфигурации к подключаемой базе данных
cmdline_args_parser.add_argument("--db_host", default="localhost:5432", help="PostgreSQL address, <ip>:<port>")
cmdline_args_parser.add_argument("--db_user", default="remote", help="PostgreSQL username")
cmdline_args_parser.add_argument("--db_pass", default="11111111", help="PostgreSQL password")
cmdline_args_parser.add_argument("--db_name", default="web-template", help="PostgreSQL db name")
cmdline_args_parser.add_argument(
    "--postgres_driver",
    default="psycopg2",
    help="PostgreSQL python driver, psycopg2 (fast, c++), pg8000 (slower, python)"
)

# Проверяем включён ли режим debug
cmdline_args_parser.add_argument("--debug", default="false", help="toggle debug mode")

cmdline_args = cmdline_args_parser.parse_args()
cmdline_args.debug = cmdline_args.debug == "false"
#########################################################################

# Путь к папку с шаблонами
template_folder = "views/"
# Gulp будет собирать две версии js/css/img и в зависимости версии будет производится сжатие или нет
static_folder = "dist-production/" if cmdline_args.debug else "dist/"

if getattr(sys, 'frozen', False):
    template_folder = os.path.join(sys._MEIPASS, template_folder)
    static_folder = os.path.join(sys._MEIPASS, static_folder)
else:
    template_folder = os.path.join('..', '..', template_folder)
    static_folder = os.path.join('..', '..', static_folder)

app = Flask(__name__, template_folder=template_folder,
            static_folder=static_folder, static_url_path="")

app.config['PROJECT'] = "web-template"
app.config['SECRET_KEY'] = 'super-secret'  # for cookies encryption
app.config['DEBUG'] = not(cmdline_args.debug)

if cmdline_args.debug:
    try:
        from flask_debugtoolbar import DebugToolbarExtension
        toolbar = DebugToolbarExtension(app)
    except ImportError:
        print 'Flask Debug Toolbar not found'

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+' + cmdline_args.postgres_driver + '://' + cmdline_args.db_user  + ':' + cmdline_args.db_pass + '@' + \
    cmdline_args.db_host + '/' + cmdline_args.db_name
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# app.config['SQLALCHEMY_ECHO'] = True  # Flag for debugging sql queries via sqlalchemy

############################################ Security ##########################################################

app.config['SECURITY_PASSWORD_SALT'] = 'super-salt'
# Месторасположение шаблона страницы авторизации
app.config['SECURITY_LOGIN_USER_TEMPLATE'] = 'user/login.pug'
# Месторасположение шаблона страницы регистрации
app.config['SECURITY_REGISTER_USER_TEMPLATE'] = 'user/register.pug'
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_SEND_REGISTER_EMAIL'] = False
app.config['SECURITY_PASSWORD_HASH'] = 'bcrypt'

#################################################################################################################

# Подключаемся к БД
db.app = app
db.init_app(app)

# From security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

#########################################################################
import application.pug_support  # nopep8
import application.views        # nopep8
#########################################################################

print( 'Run Web' )