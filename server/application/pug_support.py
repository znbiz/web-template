from application import app
from jinja2 import ext as jinja2_ext, \
    Environment as jinja_Environment
try:
    from UserDict import UserDict
except ImportError:
    from collections import UserDict
from os import path
from re import compile as re_compile, \
    escape as re_escape

##########################################################################


class MultipleReplacer(UserDict):

    def __init__(self, values=None):
        self.re = None
        self.regex = None
        UserDict.__init__(self, values)
        self.compile()

    def compile(self):
        if len(self.data) > 0:
            def escaper(s): return ' '.join(map(re_escape, s.split()))
            tmp = "(%s)" % "|".join(map(escaper, self.data.keys()))
            if self.re != tmp:
                self.re = tmp
                self.regex = re_compile(self.re)

    def __call__(self, match):
        return self.data[match.string[match.start():match.end()]]

    def sub(self, s):
        if len(self.data) == 0:
            return s
        return self.regex.sub(self, s)


class CStyleLogicalOperators(jinja2_ext.Extension):
    """Enable C-Style logical operators in templates."""
    priority = 1

    def __init__(self, env):
        jinja2_ext.Extension.__init__(self, env)
        self.replacer = MultipleReplacer({"&&": " and ",
                                          "||": " or ",
                                          "!=": "$$",
                                          "else if": "elif"})
        # todo: fix regexp to match "else if"

    def preprocess(self, source, name, filename=None):
        return self.replacer.sub(source)\
            .replace("else if", "elif")\
            .replace("!", "not ")\
            .replace("$$", "!=")


class EnvironmentWithRelativeIncludes(jinja_Environment):
    """Override join_path() to enable relative include paths."""

    def join_path(self, template, parent):
        if template.endswith(".pug"):
            platform_safe_path = path.join(*template.split('/'))
            return path.normpath(
                path.join(path.dirname(parent),
                          platform_safe_path)
            ).replace("\\", "/")
        return template

##########################################################################

app.config['TEMPLATES_AUTO_RELOAD'] = True
app.jinja_env = EnvironmentWithRelativeIncludes(
    loader=app.jinja_loader,
    extensions=[CStyleLogicalOperators]
)
app.jinja_env.add_extension('pypugjs.ext.jinja.PyPugJSExtension')
