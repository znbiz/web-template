# -*- coding: utf-8 -*-
from application import app
from extensions import db
from models.security import User, Role
from flask_security import login_required, roles_required, current_user, roles_accepted
from flask import render_template, redirect
import os.path
import datetime


@app.route( '/', methods = [ 'GET' ] )
def hello():
    # Проверяем авторизован ли пользователь
    if current_user.is_authenticated:
        return render_template( 
            'index.pug',
            page_title = 'Web шаблон'
        )
    else:
        # Редирект на страницу авторизации
        return redirect( '/login' )


# Страница доступная всем и показывающая нынешнее время
@app.route( '/time', methods = [ 'GET' ] )
def time_now():
    login_user = 'Незнакомец'
    if current_user.is_authenticated:
        login_user = current_user.email
    now = datetime.datetime.now()
    date_time = now.time()
    return render_template( 
        'time.pug',
        page_title = 'Привет!',
        login = login_user,
        date_time = date_time
    )


@app.route( '/admin', methods = [ 'GET' ] )
@login_required
# Если хотите чтобы любой авторизованный пользователь мог обратиться к этому
# контролеру то уберите ниже расположенный декоратор
@roles_required( 'admin' )
def admin_page():
    login_user = current_user.email
    return render_template( 
        'admin.pug',
        page_title = 'Страница администратора!',
        login = login_user
    )