# -*- coding: utf-8 -*-
from application import app

##########################################################################

# Настройка сервера торнадо
def main():

    try:
        from tornado.wsgi import WSGIContainer
        from tornado.httpserver import HTTPServer
        from tornado.ioloop import IOLoop

        http_server = HTTPServer(WSGIContainer(app))
        http_server.listen(5000)
        IOLoop.instance().start()
        return
    except ImportError:
        print "Tornado not found. " \
              "Failed to start tornado based server. " \
              "Falling back to flask run()."

    app.run(threaded=True)

##########################################################################

import sys

# Задаём кодировку в python на время его работы
reload(sys)
sys.setdefaultencoding('utf-8')

if __name__ == "__main__":
    main()
