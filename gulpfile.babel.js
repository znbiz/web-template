'use strict';

import gulp from 'gulp';

import print from 'gulp-print';
import flatmap from 'gulp-flatmap';
import sourcemaps from 'gulp-sourcemaps';
import changed from 'gulp-changed';
import clean from 'gulp-clean';
import util from 'gulp-util';
import concat from 'gulp-concat';
import replace from 'gulp-replace';
import plumber from 'gulp-plumber';

import webFontsBase64 from 'gulp-google-fonts-base64-css';

import stylus from 'gulp-stylus';
import autoprefixer from 'autoprefixer-stylus';
import cssmin from 'gulp-cssmin';

import {rollup} from 'rollup';
import babel from 'rollup-plugin-babel';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import uglify from 'rollup-plugin-uglify';
import strip from 'rollup-plugin-strip';

import path from 'path';

import imagemin from 'gulp-imagemin';

import browserSync from 'browser-sync';
browserSync.create();

const SRC = 'src/';
const DIST = util.env.production ? 'dist-production/' : 'dist/';

const JS = 'js/';
const CSS = 'css/';
const FONTS = 'fonts/';
const IMG = 'img/';
const PUG = 'views/';
const VENDOR = 'vendor/';


process.stdin.setMaxListeners(0);
process.stdout.setMaxListeners(0);
process.stderr.setMaxListeners(0);

const processStylus = (cssPath) =>
    () => gulp.src(SRC + cssPath + '*.styl')
        .pipe(plumber())
        .pipe(changed(DIST + CSS, {extension: '.css'}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(stylus({
            use: autoprefixer(),
            compress: util.env.production,
            'include css': true
        }))
        // remove online google fonts reference
        .pipe(replace('@import url("https://fonts.googleapis.com/css?family=Roboto:300,400,500,700");', ''))
        .pipe(util.env.production ? cssmin() : util.noop())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(DIST + cssPath))
        .pipe(browserSync.stream());

gulp.task('vendor-css', ['fonts'], processStylus(VENDOR));
gulp.task('css', processStylus(CSS));

gulp.task('fonts', () =>
    gulp.src(SRC + VENDOR + 'web-fonts.list')
        .pipe(webFontsBase64())
        .pipe(concat('web-fonts.css'))
        .pipe(gulp.dest(DIST + VENDOR))
        .pipe(browserSync.stream())
);

const ROLLUP_PLUGINS_DEV = [
    nodeResolve({jsnext: true}),
    babel({
        babelrc: false,
        presets: ['es2015-rollup'],
        exclude: 'node_modules/**'
    }),
    commonjs()
];
const ROLLUP_PLUGINS_PRODUCTION = [
    nodeResolve({jsnext: true}),
    babel({
        babelrc: false,
        presets: ['es2015-rollup'],
        exclude: 'node_modules/**'
    }),
    commonjs(),
    uglify(),
    strip()
];

const processJS = jsPath => () =>
    gulp.src(SRC + jsPath + '*.js', {read: false})
    .pipe(changed(DIST + JS))
    .pipe(flatmap((stream, file) => {
        rollup({
            entry: file.path,
            plugins: util.env.production ? ROLLUP_PLUGINS_PRODUCTION : ROLLUP_PLUGINS_DEV
        }).then(
            bundle => bundle.write({
                format: 'iife',
                dest: path.join(__dirname, DIST, jsPath, path.basename(file.path)),
                sourceMap: true,
                preferBuiltins: false
            })
        ).catch((error)=>{console.log(error)});
        return stream;
    }))
    .pipe(browserSync.stream()
);

gulp.task('js', processJS(JS));
gulp.task('vendor-js', processJS(VENDOR));

gulp.task('vendor', ['vendor-js', 'vendor-css']);

gulp.task('img',
    () => gulp.src(SRC + IMG + '**/*')
        .pipe(changed(DIST + IMG))
        .pipe(imagemin())
        .pipe(gulp.dest(DIST + IMG))
        .pipe(browserSync.stream())
);

gulp.task('pug', () => browserSync.reload());

gulp.task('browser-sync', function () {
    browserSync.init({
        proxy: 'localhost:5000',
        browser: []
    });
});


gulp.task('build', ['vendor', 'css', 'js', 'img', 'fonts']);

gulp.task('clean',
    (cb) => {
        gulp.src(DIST + '**/*', {read: false})
            .pipe(clean({read: false}));
        cb(null); // forces clean to be synchronous
    }
);

gulp.task('watch', () => {
    gulp.watch(SRC + CSS + '**/*.styl', ['css']);
    gulp.watch(SRC + JS + '**/*.js', ['js']);

    gulp.watch(SRC + VENDOR + '**/*.styl', ['vendor-css']);
    
    gulp.watch(SRC + VENDOR + '**/*.js', ['vendor-js']);

    gulp.watch(SRC + IMG + '**/*', ['img']);
    gulp.watch(PUG + '**/*.pug', ['pug']);
});


gulp.task('default', ['build', 'watch', 'browser-sync']);
